function [x] = quad_f(x, u, S)
% dynamical model of the quadrotor
% Source: https://arxiv.org/pdf/1706.06478.pdf
% Input:
%   x: state
%   u: control
%   S: parameters
% Output:
%   x: state at next time step

% Simulation/Vehicle Parameters
m = S.m;
I = S.I;
g = S.g;
e3 = [0; 0; 1];

% Control
F = u(1);
ga = u(2:4);

% State
p = [x(1); x(2); x(3)];
v = [x(4); x(5); x(6)];
ph = [x(7); x(8); x(9)];
om = [x(10); x(11); x(12)];

% Dynamical Outputs
dp = v;
dv = g*e3-F/m*R(ph)*e3;
dph = J(ph)*om;
dom = -inv(I)*hat(om)*I*om+inv(I)*ga;

% Output
dx = [dp; dv; dph; dom];

x = x + dx*S.dt;
x(7) = fangle(x(7));
x(8) = fangle(x(8));
x(9) = fangle(x(9));

end

function [j] = J(ph)
% matrix which maps from angular rate vector in body frame to dph

% From homework:
j = [cos(ph(3))/cos(ph(2)), -sin(ph(3))/cos(ph(2)), 0;
     sin(ph(3)), cos(ph(3)), 0;
     sin(ph(2))*cos(ph(3))/cos(ph(2)), sin(ph(2))*sin(ph(3))/cos(ph(2)), 1];

end