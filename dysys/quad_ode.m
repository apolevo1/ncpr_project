% Calculates ode output using backstepping controller
function [dxa] = quad_ode(t, xa, Xd, S)
% dynamical model of the quadrotor
% Source: https://arxiv.org/pdf/1706.06478.pdf
% Input:
%   x: state
%   u: control
%   S: parameters
% Output:
%   dx: dx/dt

% Simulation/Vehicle Parameters
m = S.m;
I = S.I;
g = S.g;
e3 = [0; 0; 1];

% Computing augmented state control variables (d2u, gamma)
[ua, ~] = quadrotor_bs_ctrl(t, xa, Xd, S);

% Set control
F = xa(13);
du = xa(14);
d2u = ua(1);
ga = ua(2:4);

% State
p = [xa(1); xa(2); xa(3)];
v = [xa(4); xa(5); xa(6)];
ph = [xa(7); xa(8); xa(9)];
om = [xa(10); xa(11); xa(12)];

% Dynamical Outputs
dp = v;
dv = g*e3-F/m*R(ph)*e3;
dph = J(ph)*om;
dom = -inv(I)*hat(om)*I*om+inv(I)*ga;

% Output
dxa = [dp; 
      dv; 
      dph; 
      dom;
      du;
      d2u];
  
end

function [j] = J(ph)
% matrix which maps from angular rate vector in body frame to dph

% From homework:
j = [cos(ph(3))/cos(ph(2)), -sin(ph(3))/cos(ph(2)), 0;
     sin(ph(3)), cos(ph(3)), 0;
     sin(ph(2))*cos(ph(3))/cos(ph(2)), sin(ph(2))*sin(ph(3))/cos(ph(2)), 1];

end

