%% Adam Polevoy & Nicolas Lamaison - EN.530.648 NCPR - CIS II Spring 2020

% Backstepping controller for Qcuadcopter - outputs control inputs (thrust
% and torQcue)

% Derivation approach from [1]

% Inputs:
% t - time
% x - state
% xd - desired state

function [control, t] = quadrotor_bs_ctrl(t, x, Xd, S)

% Parameters
J = S.I;
e = [0; 0; -1];
u = x(13);
du = x(14);
m = S.m;
K = S.K;
k1 = S.k1;
k2 = S.k2;
Qc = S.Qc;
grav = [0; 0; S.g];

% Calculate Parameters
A = [zeros(3), eye(3); zeros(3), zeros(3)];
B = [zeros(3); 1/m*eye(3)];
f = grav*m;

% Solves Lyapunov Equation
P = lyap((A-B*K)', Qc');

% Select point along desired trajectory
idx = uint64(round(t,-log10(S.dt_interp)) * 1/S.dt_interp + 1);
if idx > size(Xd.xd, 2)
    idx = size(Xd.xd, 2);
end
xd = Xd.xd(1:6, idx);
dxd = Xd.dxd(1:6, idx);
d2xd = Xd.d2xd(1:6, idx);
d2pd = Xd.d2xd(1:3, idx);
d3pd = Xd.d3xd(1:3, idx);
d4pd = Xd.d4xd(1:3, idx);

% State 
pv = x(1:6);
phi = x(7:9);
omega = x(10:12);

% Calculating roll-pitch-yaw rotation matrix (XYZ Euler convention)
Rot = R(phi);

% Calculate force
g = Rot*e*u;
dg = Rot*(hat(omega)*u*e+du*e);

% Calculate dx
dpv = A*pv + B*(f+g);

% Error
z0 = pv - xd;
dz0 = dpv - dxd;
gd = m*d2pd - K*z0 - f;
dgd = m*d3pd - K*dz0;
z1 = g - gd;
dz1 = dg - dgd;
ad = m*d3pd - K*dz0 - B'*P*z0 - k1*z1;
z2 = dg - ad;

% Calculate desired second derivative of control 
bd = m*d4pd - K*(A*A*pv+A*B*(f+g)+B*dg-d2xd)-B'*P*dz0-k1*dz1-z1-k2*z2;

% Calculate Control
tau = J * cross(e,(Rot'*bd - hat(omega)*hat(omega)*e*u-2*hat(omega)*e*du)/u) ...
    -cross(J * omega, omega); % - fw(s);
d2u = e'*(Rot'*bd - hat(omega)*hat(omega)*e*u-2*hat(omega)*e*du);

control = [d2u; tau];

end



