function [c] = cyl_ob(k, x, u, S)
% Cylindrical Obstacle

p = S.p;
r = S.r;
d = norm(p-x(1:2));
c = r - d;

end

