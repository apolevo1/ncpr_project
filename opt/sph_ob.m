function [c] = cyl_obs(k, x, u, S)
% Cylindrical Obstacle

c = zeros(length(S.r), 1);
for i = 1:length(S.r)
    p = S.p{i};
    r = S.r{i};
    d = norm(p-x(1:3));
    c(i, 1) = r - d;
end

end

