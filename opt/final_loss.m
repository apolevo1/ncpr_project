function [L, Lx, Lxx] = final_loss(x, S)
% cost (just standard quadratic cost)

L = (x-S.xd)'*S.Qf*(x-S.xd)/2;
Lx = S.Qf*(x-S.xd);
Lxx = S.Qf;
end
