%% Adam Polevoy and Nicolas Lamaison - EN.530.648 NCPR - CIS II Spring 2020

% Trajectory generation and tracking for quadrotor

%% Clean

% clear everything
clear all;
close all;
clc;

% add paths
addpath('../dysys');
addpath('../opt');
addpath('../utility');
addpath('../control');

% initialize rng
rng(10212)

%% Set up timing

S.dt = 0.1; % time step
S.h = S.dt; % time step
S.N = 20; % number of knot points
S.tf = S.dt*S.N; % final time
S.dt_interp = 0.00001; % time step to interpolate to

%% Set up cost/loss

% cost function parameters
S.Q = diag([0.01, 0.01, 0.01, ... % p
            0, 0, 0, ... % v
            0, 0, 0, ... % ph
            0, 0, 0]);   % om
S.R = diag([1, ...       % F
            1, 1, 1]);   % gamma
S.Qf = diag([10, 10, 10, ... % p
             1, 1, 1, ... % v
             0, 0, 0, ... % ph
             0, 0, 0]);   % om

% loss functions
S.L = @loss;
S.Lf = @final_loss;

% obstacle constraint
S.con = @sph_ob;

%% Set up dynamics

S.m = 1;
S.I = eye(3);
S.f = @quad_f;

%% Set up environment

S.g = -9.8;
S.p = {[1.5; 0; 0.75], [3.5; 1; 2.25] };
S.r = {1.1, 1.3};

%% Initialize Everything

% initial state and control
x0 = [0; 0; 0;   % p
      0; 0; 0;   % v
      0; 0; 0;   % ph
      0; 0; 0;]; % om
u0 = zeros(4,S.N);
u0(1,:) = -9.8*ones(1,S.N);

% seed control and trajectories
us = u0;
xs = traj(x0, u0, S);

% desired state and control
S.xd = [6; 0.5; 3;   % p
        0; 0; 0;   % v
        0; 0; 0;   % ph
        0; 0; 0;]; % om
S.ud = [-9.8;0;0;0];

%% generate trajectory

% Direct Collocation
[xs, us, blah, ~,~] = dircol(xs, us, S, @plottraj);

% Spline Interpolation
xs_interp = interp_traj(xs, S);

% Calculate gradients 
[xd,dxd,d2xd,d3xd,d4xd] = calc_grads(xs_interp);

% Defining structure with desired state and derivatives
Xd.t = 0:S.dt_interp:(size(xd, 2)-1)*S.dt_interp;
Xd.xd = xd; 
Xd.dxd = dxd; 
Xd.d2xd = d2xd; 
Xd.d3xd = d3xd; 
Xd.d4xd = d4xd; 

%% Tracking Gains

S.kp = 50;
S.kd = S.kp * 0.5;
S.k1 = 200;
S.k2 = 100;
S.K = [S.kp * eye(3) S.kd * eye(3)];
S.Qc = eye(6);

%% Initialize Everythin

% Perturbed initial state
e = [-0.5; 0.5; 0.5;
     0.1; 0.1; 0.1;
     0.1; 0.1; 0.1;
     0.1; 0.1; 0.1];
xp = x0 + e;

% Augmented state with dynamic compensation variables (xi1 = u, xi2 = du)
xi1_0 = -9.8;
xi2_0 = 0;
xa0 = [xp; xi1_0; xi2_0];

%% Trajectory tracking

% Simulating system
[t_as, x_as] = ode45(@quad_ode, [0 S.tf], xa0, [], Xd, S);
x_as = x_as';

%% Plot trajectory and tracking
figure;
hold on;
xlim([-1 6])
ylim([-3 4])
zlim([-1 6])
xlabel('x')
ylabel('y')
zlabel('z')

h1 = plot3(xs_interp(1, :), xs_interp(2, :), xs_interp(3,:), 'LineWidth', 3);
h2 = scatter3(xs(1, :), xs(2, :), xs(3,:), '*', 'LineWidth', 3);
h3 = scatter3(xs(1, 1), xs(2, 1), xs(3,1), 'LineWidth', 3, 'MarkerEdgeColor', 'green');
h4 = scatter3(xs(1, end), xs(2, end), xs(3,end), 'LineWidth', 3, 'MarkerEdgeColor', 'magenta');
h5 = plot3(x_as(1, :), x_as(2, :), x_as(3, :), 'LineWidth', 3, 'Color', 'k');


for i = 1:length(S.r)
    [X,Y,Z] = sphere();
    X = X * S.r{i} + S.p{i}(1);
    Y = Y * S.r{i} + S.p{i}(2);
    Z = Z * S.r{i} + S.p{i}(3);
    surf(X,Y,Z, 'EdgeColor', 'none', 'FaceColor', 'red')
    alpha 0.1
    
    [X,Y,Z] = sphere();
    X = X * (S.r{i}-0.1) + S.p{i}(1);
    Y = Y * (S.r{i}-0.1) + S.p{i}(2);
    Z = Z * (S.r{i}-0.1) + S.p{i}(3);
    surf(X,Y,Z, 'EdgeColor', 'none', 'FaceColor', 'black')
    alpha 0.1
end

legend([h1, h2, h3, h4, h5], {'Desired Trajectory', 'Knot Points', 'Start Position', 'End Position', 'Trajectory'})

figure;
clear xd_pos;
for i = 1:3
    xd_pos(i,:) = interp1(Xd.t, Xd.xd(i,:), t_as);
end
err = vecnorm(xd_pos - x_as(1:3,:));
plot(t_as, err, 'LineWidth', 3, 'Color', 'red');
xlabel('Time (s)')
ylabel('Positional Error')

figure;
hold on;
d1 = vecnorm(x_as(1:3,:) - S.p{1}) - (S.r{1}-0.1);
d2 = vecnorm(x_as(1:3,:) - S.p{2}) - (S.r{2}-0.1);
plot(t_as, d1, 'LineWidth', 3);
plot(t_as, d2, 'LineWidth', 3);
ylabel('Distance to Obstacle')
xlabel('Time (s)')
