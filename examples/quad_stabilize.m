%% Adam Polevoy and Nicolas Lamaison - EN.530.648 NCPR - CIS II Spring 2020

% Trajectory tracking for quadrotor system

%% Clean

% clear everything
clear all;
close all;
clc;

% add paths
addpath('../dysys');
addpath('../opt');
addpath('../utility');
addpath('../control');

% initialize rng
rng(10212)

%% Set up parameters

S.m = 1;
S.I = eye(3);
S.g = -9.8;

%% Gains

S.kp = 2;
S.kd = S.kp * 0.5;
S.k1 = 2;
S.k2 = 1;
S.K = [S.kp * eye(3) S.kd * eye(3)];
S.Qc = eye(6);

%% Initialize Everything

% initial state and control
x0 = [0; 0; 0;   % p
      0; 0; 0;   % v
      0; 0; 0;   % ph
      0; 0; 0;]; % om

% CONVERGING TO ORIGIN - TEST
S.dt_interp = 0.00001;
Xd.t = 0;
Xd.xd = zeros(12, 1);
Xd.dxd = zeros(12, 1);
Xd.d2xd = zeros(12, 1);
Xd.d3xd = zeros(12, 1);
Xd.d4xd = zeros(12, 1);

% Perturbed initial state
e = [-0.5; 0.5; 0.5;
     0.1; 0.1; 0.1;
     0.1; 0.1; 0.1;
     0.1; 0.1; 0.1];
xp = x0 + e;

% Augmented state with dynamic compensation variables (xi1 = u, xi2 = du)
xi1_0 = -9.8;
xi2_0 = 0;
xa0 = [xp; xi1_0; xi2_0];
  
%% Trajectory tracking

% Simulating system
[t_as, x_as] = ode45(@quad_ode, [0 10], xa0, [], Xd, S);
x_as = x_as';

%% Plot result

% Plot the position trajectory
figure;
hold on;
plot3(x_as(1, :), x_as(2, :), x_as(3, :), 'LineWidth', 3)
scatter3(xp(1), xp(2), xp(3), 'LineWidth', 3, 'MarkerEdgeColor', 'green');
scatter3(0, 0, 0, 'LineWidth', 3, 'MarkerEdgeColor', 'magenta');
xlabel('x')
ylabel('y')
zlabel('z')
legend({'Trajectory', 'Start Position', 'End Position'})

figure;
err = vecnorm(x_as(1:3,:));
plot(t_as, err, 'LineWidth', 3, 'Color', 'red');
xlabel('Time (s)')
ylabel('Positional Error')
