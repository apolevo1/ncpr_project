%% Direct Collocation for Quadrotor Dynamics

%% Clean

% clear everything
clear all;
close all;
clc;

% add paths
addpath('../dysys');
addpath('../opt');
addpath('../utility');

% initialize rng
rng(10212)

%% Set up timing

S.dt = 0.1; % time step
S.h = S.dt; % time step
S.N = 20; % number of knot points
S.tf = S.dt*S.N; % final time
S.dt_interp = 0.01; % time step to interpolate to

%% Set up cost/loss

% cost function parameters
S.Q = diag([0.01, 0.01, 0.01, ... % p
            0, 0, 0, ... % v
            0, 0, 0, ... % ph
            0, 0, 0]);   % om
S.R = diag([1, ...       % F
            1, 1, 1]);   % gamma
S.Qf = diag([10, 10, 10, ... % p
             0, 0, 0, ... % v
             0, 0, 0, ... % ph
             0, 0, 0]);   % om

% loss functions
S.L = @loss;
S.Lf = @final_loss;

%% Set up dynamics

S.m = 1;
S.I = eye(3);
S.f = @quad_f;

%% Se up environment

S.g = -9.8;
S.p = {[1.5; 0; 0.75], [3.5; 1; 2.25] };
S.r = {1, 1.2};

%% Initialize Everything

% initial state and control
x0 = [0; 0; 0;   % p
      0; 0; 0;   % v
      0; 0; 0;   % ph
      0; 0; 0;]; % om
u0 = zeros(4,S.N);
u0(1,:) = -9.8*ones(1,S.N);

% seed control and trajectories
us = u0;
xs = traj(x0, u0, S);

% desired state and control
S.xd = [6; 0.5; 3;   % p
        0; 0; 0;   % v
        0; 0; 0;   % ph
        0; 0; 0;]; % om
S.ud = [-9.8;0;0;0];

%% generate trajectory

% Direct Collocation without obstacles
[xs, us, blah, ~,~] = dircol(xs, us, S, @plottraj);

%% generate trajectory

% obstacle constraint
S.con = @sph_ob;

% Direct Collocation with obstacles
[xs, us, blah, ~,~] = dircol(xs, us, S, @plottraj);

% Spline Interpolation
xs_interp = interp_traj(xs, S);

% Calculate gradients 
[xd,dxd,d2xd,d3xd,d4xd] = calc_grads(xs_interp);

%% Plot trajectory
figure;
hold on;
xlim([-1 6])
ylim([-3 4])
zlim([-1 6])
xlabel('x')
ylabel('y')
zlabel('z')

plot3(xs(1, :), xs(2, :), xs(3,:), 'LineWidth', 3);
scatter3(xs(1, :), xs(2, :), xs(3,:), '*');

for i = 1:length(S.r)
    [X,Y,Z] = sphere();
    X = X * S.r{i} + S.p{i}(1);
    Y = Y * S.r{i} + S.p{i}(2);
    Z = Z * S.r{i} + S.p{i}(3);
    surf(X,Y,Z)
end