function xs = traj(x0, us, S)

N = size(us, 2);
xs(:,1) = x0;

for k=1:N,
  xs(:, k+1) = S.f(xs(:,k), us(:,k), S);
end
end