function [xs_interp] = interp_traj(xs, S)
%INTERP_TRAJ Interpolate trajectory using splines

ts = 0:S.dt:S.dt*(size(xs,2)-1);
ts_interp = 0:S.dt_interp:S.dt*(size(xs,2)-1);
xs_interp = zeros(size(xs,1), length(ts_interp));

for i = 1:size(xs,1)
    [x, index] = unique(xs(i,:));
    xs_interp(i,:) = interp1(ts(index), x, ts_interp, 'spline');
end

end