function [rot] = R(ph)
% rotation matrix to map vectors in body frame to vectors in inertial frame

Rx = [1,          0,           0;
      0, cos(ph(1)), -sin(ph(1));
      0, sin(ph(1)),  cos(ph(1));];
  
Ry = [ cos(ph(2)), 0, sin(ph(2));
                0, 1,          0;
      -sin(ph(2)), 0, cos(ph(2));];
  
Rz = [cos(ph(3)), -sin(ph(3)), 0;
      sin(ph(3)),  cos(ph(3)), 0;
      0,                    0, 1;];

% ZYX Euler Angles ?
rot = Rx * Ry * Rz;
  
end