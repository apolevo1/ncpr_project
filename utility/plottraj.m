function f = plottraj(xs, us, S)

%only plot every 250'th call
global pc
pc = pc + 1;
if (mod(pc,100))
  return
end

subplot(1,2,1)
% draw all obstacles
plot(xs(1,:), xs(2,:), '-b');
xlabel('x')
ylabel('y')
axis equal
drawnow 
hold on

subplot(1,2,2)
plot(0:S.h:S.tf-S.h, us(1,:),0:S.h:S.tf-S.h, us(2,:));
xlabel('sec.')
legend('u_1','u_2')
end

