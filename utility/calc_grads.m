function [xd,dxd,d2xd,d3xd,d4xd] = calc_grads(xs_interp)
%CALC_GRADS Calculate first and second derivatives

xd = xs_interp;
dxd = zeros(size(xd, 1), size(xd, 2));
d2xd = zeros(size(xd, 1), size(xd, 2));
d3xd = zeros(size(xd, 1), size(xd, 2));
d4xd = zeros(size(xd, 1), size(xd, 2));

for i = 1:size(xs_interp,1)
    dxd(i,:) = gradient(xd(i,:));
    d2xd(i,:) = gradient(dxd(i,:));
    d3xd(i,:) = gradient(d2xd(i,:));
    d4xd(i,:) = gradient(d3xd(i,:));
end

end

